%%% Classe de documento %%%
    \documentclass[12pt,openright,oneside,a4paper,brazil]{abntex2}
    
%%% Pacotes de linguagem %%%
    \usepackage[utf8]{inputenc} 
    \usepackage{lmodern}
    \usepackage[brazilian,hyperpageref]{backref}
    \usepackage[alf]{abntex2cite}
    \usepackage{fancyhdr}
		\fancyhead{}
		\fancyfoot{}
		\lhead{}
		\rhead{\thepage}
		\rfoot{}
		
%%% Comandos especiais relacionados à classe %%%
    \large\autor{Braian Montoro \\ Marina Macedo \\ Mayara Azevedo \\ Renato Santos \\ Roberto Rubens}
    \titulo{Trabalho de conclusão de curso \\ Escopo 120918} 
    \data{2018} 
    \local{São Paulo, São Paulo} 
    \tipotrabalho{monografia} 
    \orientador{Rosana Zenesi} 
    \preambulo{Monografia apresentada ao curso de Sistemas de Informação, como requisito para a obtenção do Título de Bacharel em Sistemas de Informação, Universidade Mogi das Cruzes de São Paulo}

%%% Corpo do documento %%%
\begin{document}
	\imprimircapa
	\imprimirfolhaderosto

	%%% Dedicatória - Opcional %%%
	\begin{dedicatoria} 
		\vspace*{\fill} 
		    Espaço destinado a dedicatória...
		\vspace*{\fill} 
	\end{dedicatoria} 
	
	%%% Agradecimentos - Opcional %%%
	\begin{agradecimentos} 
		Espaço destinado aos agradecimentos... 
	\end{agradecimentos} 

	%%% Epígrafe - Opcional %%% 
	\begin{epigrafe} 
		\vspace*{\fill} 
		\begin{flushright} 
			\textit{``Há aqueles que já nascem póstumos''\\ (Friedrich Wilhelm Nietzsche )} 
		\end{flushright} 
	\end{epigrafe} 
	
	%%% Resumo - Obrigatório %%%
	\begin{resumo} 
		Espaço destinado ao resumo...
		\vspace{\onelineskip} 
		\noindent
		\\ 
		\textbf{Palavras-chave}: Resumo; Trabalho; TCC; Tchau Graduação; MeTiraDaqui
	\end{resumo} 

	%%% Abstract - Obrigatório %%%
	\begin{resumo}[Abstract] 
		\begin{otherlanguage*}{english}
			Dedicated space for abstract...
			\vspace{\onelineskip}
			\\ 
			\noindent \textbf{Keywords}: Abstract; Work; TCC; Bye Graduation; SomeonePleaseHelp 
		\end{otherlanguage*} 
	\end{resumo} 
	
	%%% Lista de figuras - Opcional %%% 
	\pdfbookmark[0]{\listfigurename}{lof} \listoffigures* \cleardoublepage
	
	%%% Lista de tabelas - Opcional %%% 
	\pdfbookmark[0]{\listtablename}{lot} \listoftables* \cleardoublepage
	
	%%% Lista de siglas - Obrigatório %%% 
    \begin{siglas} 
    	\item[ONG] Organização Não Governamental
    	\item[ONU] Organização das Nações Unidas
    	\item[CNPJ] Cadastro Nacional da Pessoa Jurídica
    	\item[PNAD] Pesquisa Nacional por Amostra de Domicílios
    	\item[IBGE] Instituto Brasileiro de Geografia e Estatística
    	\item[MER] Modelo Entidade Relacionamento
    \end{siglas}
    
    %%% Sumário - Obrigatório %%%   
    \tableofcontents*
    
    %%% Elementos Textuais %%%
    \textual
    \pagestyle{fancy}
    \renewcommand{\headrulewidth}{0pt}
    \setlength{\parindent}{1.5cm}
    
    %%% CAPITULO 1 - INTRODUÇÃO %%%
    \chapter [INTRODUÇÃO]{Introdução}
        \begin{citacao}
            Considera-se serviço voluntário, para os fins desta Lei, a atividade não remunerada prestada por pessoa física a entidade pública de qualquer natureza ou a instituição privada de fins não lucrativos que tenha objetivos cívicos, culturais, educacionais, científicos, recreativos ou de assistência à pessoa. \cite{lei}
        \end{citacao}
        \hspace{1.37cm} No Brasil, o trabalho voluntário, além de ser uma atividade estipulada em lei, tem também uma data para sua comemoração, 28 de agosto, o Dia Nacional do Voluntariado, definida pela Lei nº 7.352 de 1985 \cite{leidia}. Apesar disso, de acordo com o site revista Pré Univesp \cite{univesp}, a participação da população brasileira em ações de voluntariado ainda é pequena. 

    Existem diversas formas de atuação para um trabalho voluntário, que variam de presenciais ou à distância, através de ações individuais (médicos, advogados, dentistas); participação de campanhas (doação de sangue, arrecadação de livros, reciclagem); criação de grupos para apoio ou suporte (associação de moradores, grupo de trabalhos com objetivos como saneamento e saúde, apoio para pessoas com depressão); atuação em projetos públicos com objetivo de melhoria na cidade (plantar árvores, multidões de limpeza das ruas e praias). \cite{maturidade}

    A prática de ações voluntárias em primeiro momento nos faz relacionar às ONGs. A expressão “Organização não Governamental” foi empregada pela primeira vez no ano de 1950, pela Organização das Nações Unidas (ONU), para fazer referência às organizações civis que não tinham nenhum vínculo com o governo. São todas as organizações, sem fins lucrativos, caracterizadas por ações de solidariedade, atuam em todas as áreas ligadas às necessidades sociais que existam, e são normalmente mantidas por doações e por financiamento privado. Para sua criação ela deve ser registrada em Cartório, ser cadastrada no Cadastro Nacional da Pessoa Jurídica (CNPJ) e ter o seu modo de funcionamento definido em um regulamento, podendo ser fundada como uma associação ou uma fundação.

    Porém, a organização de ações voluntárias não inicia-se somente por ONGs, mas também são efetuadas por pessoas comuns que unem-se pela mesma causa e não possui vínculo com nenhuma organização não-governamental, como associação de moradores, instituições religiosas e até em grupos de redes sociais.

    Em 2016 um estudo da Pesquisa Nacional por Amostra de Domicílios (PNAD), do Instituto Brasileiro de Geografia e Estatística (IBGE), mostrou que apenas 3,9\% da população maior de 14 anos realizou algum tipo de trabalho voluntário no Brasil. 
    
    \vspace{2.0cm} %%% Força o Texto ir a proxima página %%%
    
    \begin{citacao} "A falta de divulgação das possibilidades de voluntariado também contribui para que poucas pessoas tenham conhecimento sobre as atividades realizadas em ONGs e projetos voltados às comunidades." \cite{moraes}\footnote{Valquiria Moraes, psicóloga e presidente da ONG Pequeno Cidadão.}\end{citacao}

    Uma pesquisa do Datafolha realizada em dezembro de 2014 ouviu 2.024 pessoas em 135 municípios. Entre os motivos para não ser voluntário, “falta de tempo” foi o motivo alegado por 40\% dos entrevistados. Outras razões apontadas foram: “nunca foram convidados” (29\%), “nunca pensaram nessa possibilidade” (18\%) e “não sabem onde obter informações sobre isso” (12\%). \cite{g1}

    Com base em dados que obtivemos em uma pesquisa via formulário eletrônico para mais de 200 pessoas, distribuídos entre: Linkedin, Facebook e fóruns de discussões, que abordam diversos assuntos relacionados ao dia a dia, desde facilidades em realizar algumas tarefas e até hobbies diversos, 52,6\% das pessoas que participam ou participaram ficam sabendo apenas por conhecidos, 34,9\% gostariam futuramente de organizar um evento para ajudar sua causa e 44,9\% não participam de ações voluntárias porque não encontram facilmente divulgações. A pesquisa baseia-se nas opiniões de pessoas de diferentes classes sociais, estilos e comportamentos, pois foi publicada separadamente pelos cinco integrantes do grupo que possuem interesses completamente diferentes. 

    Atualmente existem poucas plataformas para criação e divulgação de eventos sociais em busca de voluntários. Na fase de pesquisa do projeto efetuamos testes em três plataformas já existentes, são elas: voluntarios.com.br, voluntariado.org.br e atados.com.br.  

    Os sites voluntarios.com.br e voluntariado.org.br não exibem eventos, apenas lista e exibe contatos de ONGs relacionados a causa que é buscada. Já atados.com.br é uma ferramenta mais complexa, que permite buscar oportunidades de trabalhos voluntários, exibem os eventos próximos e permitem que os voluntários candidatem-se para o evento. Porém a criação de eventos só é permitida por líderes de ONGs oficiais e cadastradas.  

    Em resumo, de acordo com estatísticas e depoimentos informados acima, a divulgação e a centralização de informações com relação a atividades voluntárias são escassas e as plataformas existentes no mercado não permitem pessoas sem vínculos com ONGs terem a oportunidade de criarem seus próprios eventos, o que deixa uma abertura para que novas tecnologias possam surgir para atender esses déficits.
    
    \vspace{2cm} %%% Força o Texto ir a proxima página %%%
    
    %%% Seção 1.1 OBJETTIVO %%%
    \section{Objetivo}

    \hspace{1.37cm} O projeto teve como objetivo desenvolver uma aplicação Web que permite o cadastro de voluntários, cadastro de eventos voluntários, divulgação destes eventos em redes sociais como LinkedIn, Twitter e Facebook, busca de eventos voluntários pela localização ou área de interesse e a candidatura nesses eventos.

    %%% Seção 1.2 JUSTIFICATIVA %%%
    \section{Justificativa}

    \hspace{1.37cm} Com a evolução da tecnologia é notável que estamos cada vez mais dependentes de aparelhos eletrônicos, das suas funcionalidades e praticidades. Será que não poderíamos utilizar a tecnologia para motivar atitudes voluntárias nas pessoas? Esse questionamento nos levou a refletir e através dele surgiu a ideia do Help-Us.

    Nosso software propõe maior facilidade para aqueles que buscam prestar uma ação voluntária devido à carência ao acesso de informações e divulgação dos mesmos. 

    O projeto possuirá diversas funcionalidades, desde o cadastro de eventos por pessoa física ou jurídica, busca de eventos voluntários por localidade ou área de interesse, candidatura a esses eventos e um histórico sobre aqueles em que você já participou.
    
    O nosso diferencial é justamente unificar em uma só plataforma todas as informações sobre eventos voluntários para aqueles que desejam criar ou participar.
    
    %%% Seção 1.3 METODOLOGIA %%%
    \section{Metodologia}
    Etapa 1 – Pesquisas sobre ações voluntárias, fundamentação e definição do problema. 
    \begin{itemize}
    	\item Foram feitas pesquisas para um melhor entendimento do que se trata uma ação voluntária, mediante a Lei nº 9.608 \cite{lei}, dados divulgados pelo Instituto Brasileiro de Geografia e Estatística (IBGE) e foi feita uma coleta de opiniões de diversas pessoas através de redes sociais distintas, onde chegamos à conclusão de que há uma defasagem e falta de informação, acesso e divulgação centralizada para pessoas que se interessam em prestar uma ação voluntária.
    \end{itemize}
    Etapa 2 – Detalhamento das funções do sistema, definição dos processos e automatização da informação.
    \begin{itemize}
    	\item Através da definição do problema proposta na etapa 1, foi feito um detalhamento geral de como o sistema funciona, através do fluxo de processos necessários e a troca de informações. Chegamos a um levantamento de requisitos gerais do sistema.
    \end{itemize}
	\vspace{2cm} %%% Força o Texto ir a proxima página %%%
    Etapa 3 – Definição dos requisitos funcionais e requisitos não funcionais.
    \begin{itemize}
    	\item Foi feito o levantamento de todos os requisitos funcionais e não funcionais do sistema e a análise da viabilidade de cada requisito. Baseado na visão geral do sistema, presente na etapa 2, obtivemos como resultado uma lista de requisitos prontos para serem utilizados na próxima etapa.
    \end{itemize}
    Etapa 4 – Modelagem do sistema, casos de uso detalhado e diagramas.
    \begin{itemize}
    	\item Nesta etapa foi feito toda a modelagem do sistema, começando pelo diagrama de caso de uso, seguido do diagrama de classes e diagrama de sequência. Também foi feito o diagrama de entidade relacionamento (MER). Os diagramas foram extraídos a partir dos requisitos funcionais e não funcionais definidos na etapa 3, e resultaram em uma visão gráfica das interações do usuário com o sistema, ditada pelas funcionalidades e processos disponíveis.
    \end{itemize}
    Etapa 5 – Protótipo das interfaces para interação do usuário com o sistema.
    \begin{itemize}
    	\item Foi feita a prototipagem do sistema, definição de telas visuais para interação do usuário e suas funcionalidades de maneira gráfica e usual. O protótipo foi baseado na documentação do sistema, em seus requisitos funcionais e não funcionais necessários para usabilidade do usuário;
    	\item Resultou em uma visualização da versão final do sistema, com todas as suas telas e funcionalidades definidas, possibilitando uma fácil visualização gráfica.  
    \end{itemize}
    Etapa 6 – Implementação, criação da base de dados e cronograma de testes.
    \begin{itemize}
    	\item Foi definido todo ambiente para desenvolvimento do sistema e o cronograma de testes. A implementação foi baseada na facilidade que a equipe de desenvolvimento já possuí em manipular as ferramentas escolhidas e o cronograma de testes é baseado em todas as funcionalidades que o sistema possui;
    	\item Esta etapa resultou na versão final do sistema com a maior parte das funcionalidades já implementadas, o sistema possui no mínimo 80\% de seu funcionamento.
    \end{itemize}
    Etapa 7 – Conclusão, correção de bugs e entrega do projeto.
    \begin{itemize}
    	\item Foi feita a conclusão do sistema, com todas as correções necessárias e a apresentação e entrega do mesmo, baseado nos testes realizados na etapa 6, foi possível corrigir qualquer funcionalidade que estava incorreta ou incoerente com os requisitos definidos; 
    	\item Esta etapa resultou na entrega final do projeto ao cliente, com 100\% do sistema implantado e funcionado.
    \end{itemize}
        
    %%% Seção 1.4 ESTRUTURA DO TRABALHO %%%
    \section{Estrutura do Trabalho}
    \hspace{1.37cm} Para melhor compreensão deste trabalho, o mesmo foi separado e organizado em 6 (seis) capítulos.

    O capítulo 2 - Visão Geral do Sistema - fornece uma análise geral do sistema, apresentando de forma sucinta o que foi desenvolvido, juntamente com seus devidos conceitos e definições de forma que facilite o entendimento do sistema.

    O capítulo 3 - Requisitos - dedica-se a evidenciar os requisitos e seus conceitos, e como o sistema se comporta diante do que foi descrito.

    O capítulo 4 - Modelagem - apresenta o segmento de modelagem estrutural que compõe o sistema, contendo as representações dos diferentes aspectos evidenciados no aplicativo.

    O capítulo 5 - Protótipo - expõe o protótipo operacional do sistema, acentuando as explicações das suas respectivas funcionalidades neste trabalho.

    O capítulo 6 - Programação e Testes - aborda o elemento lógico do sistema, as práticas, métricas e conceitos utilizados para o desenvolvimento, demonstrando a rotina de testes executadas para garantir a confiabilidade do sistema desenvolvido.

    No capítulo 7 - Conclusão - estão as considerações finais e conclusões sobre o desenvolvimento do presente trabalho.

    \bibliography{referencias}

%%% Fim do Documento %%%
\end{document}