## CHANGELOG 

###### 04/09/2018
 - Versão Inicial

###### 05/09/2018
 - Alteração na Margem (1.5cm)
 - Alteração na citação (recuado)
 - Objetivo reescrito
 - Adicionado algumas siglas

###### 06/09/2018
 - Cabeçalhos alterados 
 
###### 12/09/2018 - Release 12 Setembro
 - Alterações na margem
 - Alterações no comportamento do texto na pagina
 - Conteudo do capitulo reescrito
 - Sigla em desuso removida
 - Inclusão de referencias